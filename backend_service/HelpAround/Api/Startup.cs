using Application.Common;
using Application.Dependants;
using Application.Interfaces;
using Application.Products;
using Application.Providers;
using Application.Transactions;
using Application.Voluntaries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient <IDatabaseService, DatabaseService>();

            services.AddTransient<ILocalityService, LocalityService>();
            services.AddTransient<IProvinceService, ProvinceService>();
            services.AddTransient<IAddressService, AddressService>();

            services.AddTransient<IDependantSituationService, DependantSituationService>();
            services.AddTransient<IDependantTypeService, DependantTypeService>();
            services.AddTransient<IDependantService, DependantService>();

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProviderService, ProviderService>();
            services.AddTransient<IProductProviderService, ProductProviderService>();

            services.AddTransient<IVoluntaryService, VoluntaryService>();

            services.AddTransient<ITransactionStatusService, TransactionStatusService>();
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<ITransactionDetailService, TransactionDetailService>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
