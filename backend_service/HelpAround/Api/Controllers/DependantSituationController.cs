﻿using Application.Dependants;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DependantSituationController : ControllerBase
    {
        private IDependantSituationService _dependantSituationService;
        public DependantSituationController(IDependantSituationService dependantSituationService)
        {
            _dependantSituationService = dependantSituationService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var dependantSituations = _dependantSituationService.GetAll();

            return Ok(dependantSituations);
        }
    }
}
