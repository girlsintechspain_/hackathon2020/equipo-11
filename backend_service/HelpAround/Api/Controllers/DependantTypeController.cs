﻿using Application.Dependants;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DependantTypeController : ControllerBase
    {
        private IDependantTypeService _dependantTypeService;
        public DependantTypeController(IDependantTypeService dependantTypeService)
        {
            _dependantTypeService = dependantTypeService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var dependantTypes = _dependantTypeService.GetAll();

            return Ok(dependantTypes);
        }
    }
}
