﻿using Application.Transactions;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionStatusController : ControllerBase
    {
        private ITransactionStatusService _transactionStatusService;
        public TransactionStatusController(ITransactionStatusService transactionStatusService)
        {
            _transactionStatusService = transactionStatusService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var dependantTypes = _transactionStatusService.GetAll();

            return Ok(dependantTypes);
        }
    }
}
