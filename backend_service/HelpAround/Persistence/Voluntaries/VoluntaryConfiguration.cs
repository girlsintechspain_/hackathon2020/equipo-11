﻿using Domain.Common;
using Domain.Voluntaries;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Voluntaries
{
    public class VoluntaryConfiguration : IEntityTypeConfiguration<Voluntary>
    {
        public void Configure(EntityTypeBuilder<Voluntary> builder)
        {
            builder.ToTable(nameof(Voluntary));
            builder.HasKey(t => t.VoluntaryId);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Voluntary>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
