﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Common
{
    public class ProvinceConfiguration : IEntityTypeConfiguration<Province>
    {
        public void Configure(EntityTypeBuilder<Province> builder)
        {
            builder.ToTable(nameof(Province));
            builder.HasKey(t => t.ProvinceId);

            builder.Property(t => t.Name)
                .IsRequired();

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Province>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
