﻿using Domain.Common;
using Domain.Dependants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace Persistence.Dependants
{
    public class DependantTypeConfiguration : IEntityTypeConfiguration<DependantType>
    {
        public void Configure(EntityTypeBuilder<DependantType> builder)
        {
            builder.ToTable(nameof(DependantType));
            builder.HasKey(t => t.DependantTypeId);

            builder.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(e => e.DependantTypeId)
                .HasConversion<int>();

            builder.HasData(
                Enum.GetValues(typeof(DependantType.DependantTypeEnum))
                    .Cast<DependantType.DependantTypeEnum>()
                    .Select(e => new DependantType()
                    {
                        DependantTypeId = e,
                        Value = e.ToString()
                    })
            );
        }
    }
}
