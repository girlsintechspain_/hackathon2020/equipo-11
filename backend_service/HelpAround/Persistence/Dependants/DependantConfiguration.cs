﻿using Domain.Common;
using Domain.Dependants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Dependants
{
    public class DependantConfiguration : IEntityTypeConfiguration<Dependant>
    {
        public void Configure(EntityTypeBuilder<Dependant> builder)
        {
            builder.ToTable(nameof(Dependant));
            builder.HasKey(t => t.DependantId);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(e => e.DependantSituationId)
                .HasConversion<int>();

            builder.Property(e => e.DependantTypeId)
                .HasConversion<int>();

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Dependant>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(c => c.DependantSituation)
                .WithMany()
                .HasForeignKey(c => c.DependantSituationId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(c => c.DependantType)
                .WithMany()
                .HasForeignKey(c => c.DependantTypeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
