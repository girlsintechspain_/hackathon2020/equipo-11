﻿using Domain.Common;
using Domain.Dependants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace Persistence.Dependants
{
    public class DependantSituationConfiguration : IEntityTypeConfiguration<DependantSituation>
    {
        public void Configure(EntityTypeBuilder<DependantSituation> builder)
        {
            builder.ToTable(nameof(DependantSituation));
            builder.HasKey(t => t.DependantSituationId);

            builder.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(e => e.DependantSituationId)
                .HasConversion<int>();

            builder.HasData(
                Enum.GetValues(typeof(DependantSituation.DependantSituationEnum))
                    .Cast<DependantSituation.DependantSituationEnum>()
                    .Select(e => new DependantSituation()
                    {
                        DependantSituationId = e,
                        Value = e.ToString()
                    })
            );
        }
    }
}
