﻿using Domain.Common;
using Domain.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Products
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(nameof(Product));
            builder.HasKey(t => t.ProductId);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Product>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
