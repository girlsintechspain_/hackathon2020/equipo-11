﻿using Domain.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Products
{
    public class ProductProviderConfiguration : IEntityTypeConfiguration<ProductProvider>
    {
        public void Configure(EntityTypeBuilder<ProductProvider> builder)
        {
            builder.ToTable(nameof(ProductProvider));
            builder.HasKey(t => new { t.ProductId, t.ProviderId });

            builder.HasOne(t => t.Product)
                .WithMany(t => t.ProductProvider)
                .HasForeignKey(t => t.ProductId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Provider)
                .WithMany(t => t.ProductProvider)
                .HasForeignKey(t => t.ProviderId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
