﻿using Domain.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Transactions
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.ToTable(nameof(Transaction));
            builder.HasKey(t => t.TransactionId);

            builder.Property(e => e.TransactionStatusId)
                .HasConversion<int>();

            builder.HasOne(t => t.Voluntary)
                .WithMany(t => t.Transactions)
                .HasForeignKey(t => t.VoluntaryId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Dependant)
                .WithMany(t => t.Transactions)
                .HasForeignKey(t => t.DependantId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(c => c.TransactionStatus)
                .WithMany()
                .HasForeignKey(c => c.TransactionStatusId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
