﻿using Domain.Common;
using Domain.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace Persistence.Transactions
{
    public class TransactionStatusConfiguration : IEntityTypeConfiguration<TransactionStatus>
    {
        public void Configure(EntityTypeBuilder<TransactionStatus> builder)
        {
            builder.ToTable(nameof(TransactionStatus));
            builder.HasKey(t => t.TransactionStatusId);

            builder.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(e => e.TransactionStatusId)
                .HasConversion<int>();

            builder.HasData(
                Enum.GetValues(typeof(TransactionStatus.TransactionStatusEnum))
                    .Cast<TransactionStatus.TransactionStatusEnum>()
                    .Select(e => new TransactionStatus()
                    {
                        TransactionStatusId = e,
                        Value = e.ToString()
                    })
            );
        }
    }
}
