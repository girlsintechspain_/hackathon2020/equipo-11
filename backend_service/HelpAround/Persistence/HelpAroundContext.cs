﻿using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using Domain.Providers;
using Domain.Transactions;
using Domain.Voluntaries;
using Microsoft.EntityFrameworkCore;
using Persistence.Common;
using Persistence.Dependants;
using Persistence.Products;
using Persistence.Providers;
using Persistence.Transactions;
using Persistence.Voluntaries;

namespace Persistence
{
    public class HelpAroundContext : DbContext
    {
        public HelpAroundContext(DbContextOptions<HelpAroundContext> options) : base (options)
        {

        }
        public DbSet<ModelBase> ModelBase { get; set; }
        public DbSet<Province> Province { get; set; }
        public DbSet<Locality> Locality { get; set; }
        public DbSet<Address> Address { get; set; }

        public DbSet<Dependant> Dependant { get; set; }
        public DbSet<DependantSituation> DependantSituation { get; set; }
        public DbSet<DependantType> DependantType { get; set; }
        public DbSet<Voluntary> Voluntary { get; set; }
        public DbSet<Provider> Provider { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductProvider> ProductProvider { get; set; }

        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionDetail> TransactionDetail { get; set; }
        public DbSet<TransactionStatus> TransactionStatus { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LocalityConfiguration());
            modelBuilder.ApplyConfiguration(new ProvinceConfiguration());
            modelBuilder.ApplyConfiguration(new AddressConfiguration());

            modelBuilder.ApplyConfiguration(new DependantConfiguration());
            modelBuilder.ApplyConfiguration(new DependantSituationConfiguration());
            modelBuilder.ApplyConfiguration(new DependantTypeConfiguration());

            modelBuilder.ApplyConfiguration(new VoluntaryConfiguration());
            modelBuilder.ApplyConfiguration(new ProviderConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductProviderConfiguration());

            modelBuilder.ApplyConfiguration(new TransactionConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionDetailConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionStatusConfiguration());

            modelBuilder.ApplyConfiguration(new ModelBaseConfiguration());
        }
    }
}
