﻿using Domain.Common;
using Domain.Providers;
using Domain.Transactions;
using System.Collections.Generic;

namespace Domain.Products
{
    public class Product
    {
        public Product()
        {
            ProductProvider = new List<ProductProvider>();
            TransactionDetails = new List<TransactionDetail>();
        }
        public long ProductId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public List<ProductProvider> ProductProvider { get; set; }
        public List<TransactionDetail> TransactionDetails { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public double Price { get; set; }
    }
}
