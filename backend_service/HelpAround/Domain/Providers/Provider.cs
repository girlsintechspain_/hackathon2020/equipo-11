﻿using Domain.Common;
using Domain.Products;
using Domain.Transactions;
using System;
using System.Collections.Generic;

namespace Domain.Providers
{
    public class Provider
    {
        public Provider()
        {
            ProductProvider = new List<ProductProvider>();
            TransactionDetails = new List<TransactionDetail>();
        }
        public long ProviderId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public List<ProductProvider> ProductProvider { get; set; }
        public List<TransactionDetail> TransactionDetails { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool IsAvailable { get; set; }
        public TimeSpan? Open { get; set; }
        public TimeSpan? Close { get; set; }
    }
}
