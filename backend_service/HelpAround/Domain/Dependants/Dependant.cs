﻿using Domain.Common;
using Domain.Transactions;
using System.Collections.Generic;

namespace Domain.Dependants
{
    public class Dependant
    {
        public Dependant()
        {
            Addresses = new List<Address>();
            Transactions = new List<Transaction>();
        }
        public long DependantId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public string Name { get; set; }
        public List<Address> Addresses { get; set; }
        public List<Transaction> Transactions { get; set; }
        public DependantType.DependantTypeEnum DependantTypeId { get; set; }
        public DependantType DependantType { get; set; }
        public DependantSituation.DependantSituationEnum DependantSituationId { get; set; }
        public DependantSituation DependantSituation { get; set; }
        public string Phone { get; set; }
    }
}
