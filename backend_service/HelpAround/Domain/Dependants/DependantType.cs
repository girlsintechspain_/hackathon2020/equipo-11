﻿using Domain.Common;

namespace Domain.Dependants
{
    public class DependantType
    {
        public enum DependantTypeEnum
        {
            Normal = 1
            , Risk = 2
        }
        public DependantTypeEnum DependantTypeId { get; set; }
        public string Value { get; set; }
    }
}
