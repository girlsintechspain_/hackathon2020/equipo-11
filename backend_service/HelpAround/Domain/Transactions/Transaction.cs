﻿using Domain.Dependants;
using Domain.Voluntaries;
using System;
using System.Collections.Generic;

namespace Domain.Transactions
{
    public class Transaction
    {
        public Transaction()
        {
            TransactionDetails = new List<TransactionDetail>();
        }
        public long TransactionId { get; set; }
        public long VoluntaryId { get; set; }
        public Voluntary Voluntary { get; set; }
        public long DependantId { get; set; }
        public Dependant Dependant { get; set; }
        public List<TransactionDetail> TransactionDetails { get; set; }
        public TransactionStatus.TransactionStatusEnum TransactionStatusId { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsAllowProductChange { get; set; }
    }
}
