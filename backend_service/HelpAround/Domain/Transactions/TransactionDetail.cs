﻿using Domain.Products;
using Domain.Providers;

namespace Domain.Transactions
{
    public class TransactionDetail
    {
        public long TransactionDetailId { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public long ProviderId { get; set; }
        public Provider Provider { get; set; }
        public long TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}
