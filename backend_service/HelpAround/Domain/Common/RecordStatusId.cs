﻿namespace Domain.Common
{
    public enum RecordStatusId
    {
        Active = 1
        , Inactive = 2
    }
}
