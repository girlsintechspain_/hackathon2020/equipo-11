﻿using System.Collections.Generic;

namespace Domain.Common
{
    public class Locality
    {
        public Locality()
        {
            Addresses = new List<Address>();
        }
        public int LocalityId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public int ProvinceId { get; set; }
        public Province Province { get; set; }
        public List<Address> Addresses { get; set; }
        public string Name { get; set; }
    }
}
