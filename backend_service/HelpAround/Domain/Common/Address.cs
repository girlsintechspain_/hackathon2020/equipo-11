﻿using Domain.Dependants;

namespace Domain.Common
{
    public class Address
    {
        public long AddressId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public int ProvinceId { get; set; }
        public Province Province { get; set; }
        public int LocalityId { get; set; }
        public Locality Locality { get; set; }
        public long DependantId { get; set; }
        public Dependant Dependant { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public int? Portal { get; set; }
        public string Stairs { get; set; }
        public int? Floor { get; set; }
        public string Door { get; set; }

    }
}
