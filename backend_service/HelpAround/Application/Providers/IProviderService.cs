﻿using Domain.Providers;
using System.Collections.Generic;

namespace Application.Providers
{
    public interface IProviderService
    {
        public List<Provider> GetAll();
        public Provider Get(long id);
        public Provider Add(Provider provider);
        public Provider Update(Provider provider);
    }
}
