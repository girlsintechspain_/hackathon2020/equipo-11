﻿using Domain.Dependants;
using System.Collections.Generic;

namespace Application.Dependants
{
    public interface IDependantTypeService
    {
        public List<DependantType> GetAll();
        public DependantType Get(long id);
        public DependantType Add(DependantType dependantType);
        public DependantType Update(DependantType dependantType);
    }
}
