﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Dependants
{
    public class DependantService : IDependantService
    {
        private IDatabaseService _databaseService;
        public DependantService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Dependant Add(Dependant dependant)
        {
            _databaseService.Dependant.Add(dependant);
            _databaseService.Save();

            return dependant;
        }

        public List<Dependant> GetAll()
        {
            return _databaseService.Dependant.ToList();
        }

        public Dependant Get(long id)
        {
            var dependant = _databaseService.Dependant.Single(a => a.DependantId == id);
            return dependant;
        }

        public Dependant Update(Dependant dependant)
        {
            _databaseService.Dependant.Update(dependant);
            _databaseService.Save();

            return dependant;
        }
    }
}
