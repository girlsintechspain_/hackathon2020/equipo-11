﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Dependants
{
    public class DependantTypeService : IDependantTypeService
    {
        private IDatabaseService _databaseService;
        public DependantTypeService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public DependantType Add(DependantType dependantType)
        {
            _databaseService.DependantType.Add(dependantType);
            _databaseService.Save();

            return dependantType;
        }

        public List<DependantType> GetAll()
        {
            return _databaseService.DependantType.ToList();
        }

        public DependantType Get(long id)
        {
            var dependantType = _databaseService.DependantType.Single(a => Convert.ToInt64(a.DependantTypeId) == id);
            return dependantType;
        }

        public DependantType Update(DependantType dependantType)
        {
            _databaseService.DependantType.Update(dependantType);
            _databaseService.Save();

            return dependantType;
        }
    }
}
