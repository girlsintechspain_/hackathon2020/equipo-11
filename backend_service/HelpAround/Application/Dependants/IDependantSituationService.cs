﻿using Domain.Dependants;
using System.Collections.Generic;

namespace Application.Dependants
{
    public interface IDependantSituationService
    {
        public List<DependantSituation> GetAll();
        public DependantSituation Get(long id);
        public DependantSituation Add(DependantSituation dependantSituation);
        public DependantSituation Update(DependantSituation dependantSituation);
    }
}
