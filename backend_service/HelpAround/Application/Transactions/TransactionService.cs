﻿using Application.Interfaces;
using Domain.Providers;
using Domain.Transactions;
using System.Collections.Generic;
using System.Linq;

namespace Application.Transactions
{
    public class TransactionService : ITransactionService
    {
        private IDatabaseService _databaseService;
        public TransactionService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Transaction Add(Transaction transaction)
        {
            _databaseService.Transaction.Add(transaction);
            _databaseService.Save();

            return transaction;
        }

        public List<Transaction> GetAll()
        {
            return _databaseService.Transaction.ToList();
        }

        public Transaction Get(long id)
        {
            var transaction = _databaseService.Transaction.Single(a => a.TransactionId == id);
            return transaction;
        }

        public Transaction Update(Transaction transaction)
        {
            _databaseService.Transaction.Update(transaction);
            _databaseService.Save();

            return transaction;
        }
    }
}
