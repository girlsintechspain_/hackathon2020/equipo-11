﻿using Application.Interfaces;
using Domain.Providers;
using Domain.Transactions;
using System.Collections.Generic;
using System.Linq;

namespace Application.Transactions
{
    public class TransactionDetailService : ITransactionDetailService
    {
        private IDatabaseService _databaseService;
        public TransactionDetailService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public TransactionDetail Add(TransactionDetail transactionDetail)
        {
            _databaseService.TransactionDetail.Add(transactionDetail);
            _databaseService.Save();

            return transactionDetail;
        }

        public List<TransactionDetail> GetAll()
        {
            return _databaseService.TransactionDetail.ToList();
        }

        public TransactionDetail Get(long id)
        {
            var transactionDetail = _databaseService.TransactionDetail.Single(a => a.TransactionDetailId == id);
            return transactionDetail;
        }

        public TransactionDetail Update(TransactionDetail transactionDetail)
        {
            _databaseService.TransactionDetail.Update(transactionDetail);
            _databaseService.Save();

            return transactionDetail;
        }
    }
}
