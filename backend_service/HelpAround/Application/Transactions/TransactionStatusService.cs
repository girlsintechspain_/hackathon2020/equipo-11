﻿using Application.Interfaces;
using Domain.Providers;
using Domain.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Transactions
{
    public class TransactionStatusService : ITransactionStatusService
    {
        private IDatabaseService _databaseService;
        public TransactionStatusService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public TransactionStatus Add(TransactionStatus transactionStatus)
        {
            _databaseService.TransactionStatus.Add(transactionStatus);
            _databaseService.Save();

            return transactionStatus;
        }

        public List<TransactionStatus> GetAll()
        {
            return _databaseService.TransactionStatus.ToList();
        }

        public TransactionStatus Get(long id)
        {
            var transactionStatus = _databaseService.TransactionStatus.Single(a => Convert.ToInt64(a.TransactionStatusId) == id);
            return transactionStatus;
        }

        public TransactionStatus Update(TransactionStatus transactionStatus)
        {
            _databaseService.TransactionStatus.Update(transactionStatus);
            _databaseService.Save();

            return transactionStatus;
        }
    }
}
