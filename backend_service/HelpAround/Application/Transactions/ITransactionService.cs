﻿using Domain.Providers;
using Domain.Transactions;
using System.Collections.Generic;

namespace Application.Transactions
{
    public interface ITransactionService
    {
        public List<Transaction> GetAll();
        public Transaction Get(long id);
        public Transaction Add(Transaction transaction);
        public Transaction Update(Transaction transaction);
    }
}
