﻿using Domain.Voluntaries;
using System.Collections.Generic;

namespace Application.Voluntaries
{
    public interface IVoluntaryService
    {
        public List<Voluntary> GetAll();
        public Voluntary Get(long id);
        public Voluntary Add(Voluntary voluntary);
        public Voluntary Update(Voluntary voluntary);
    }
}
