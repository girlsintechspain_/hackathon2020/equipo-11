﻿using Application.Interfaces;
using Domain.Common;
using System.Collections.Generic;
using System.Linq;

namespace Application.Common
{
    public class ProvinceService : IProvinceService
    {
        private IDatabaseService _databaseService;
        public ProvinceService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Province Add(Province locality)
        {
            _databaseService.Province.Add(locality);
            _databaseService.Save();

            return locality;
        }

        public List<Province> GetAll()
        {
            return _databaseService.Province.ToList();
        }

        public Province Get(long id)
        {
            var province = _databaseService.Province.Single(a => a.ProvinceId == id);
            return province;
        }

        public Province Update(Province province)
        {
            _databaseService.Province.Update(province);
            _databaseService.Save();

            return province;
        }
    }
}
