﻿using Application.Interfaces;
using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common
{
    public class LocalityService: ILocalityService
    {
        private IDatabaseService _databaseService;
        public LocalityService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Locality Add(Locality locality)
        {
            _databaseService.Locality.Add(locality);
            _databaseService.Save();

            return locality;
        }

        public List<Locality> GetAll()
        {
            return _databaseService.Locality.ToList();
        }

        public Locality Get(long id)
        {
            var locality = _databaseService.Locality.Single(a => a.LocalityId == id);
            return locality;
        }

        public Locality Update(Locality locality)
        {
            _databaseService.Locality.Update(locality);
            _databaseService.Save();

            return locality;
        }
    }
}
