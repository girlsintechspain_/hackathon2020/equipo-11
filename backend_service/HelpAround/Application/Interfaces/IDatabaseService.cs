﻿using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using Domain.Providers;
using Domain.Transactions;
using Domain.Voluntaries;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces
{
    public interface IDatabaseService
    {
        public DbSet<ModelBase> ModelBase { get; set; }
        public DbSet<Province> Province { get; set; }
        public DbSet<Locality> Locality { get; set; }
        public DbSet<Address> Address { get; set; }

        public DbSet<Dependant> Dependant { get; set; }
        public DbSet<DependantSituation> DependantSituation { get; set; }
        public DbSet<DependantType> DependantType { get; set; }
        public DbSet<Voluntary> Voluntary { get; set; }
        public DbSet<Provider> Provider { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductProvider> ProductProvider { get; set; }

        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionDetail> TransactionDetail { get; set; }
        public DbSet<TransactionStatus> TransactionStatus { get; set; }

        public void Save();
    }
}
