﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Products
{
    public class ProductProviderService : IProductProviderService
    {
        private IDatabaseService _databaseService;
        public ProductProviderService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public ProductProvider Add(ProductProvider productProvider)
        {
            _databaseService.ProductProvider.Add(productProvider);
            _databaseService.Save();

            return productProvider;
        }

        public void Remove(ProductProvider productProvider)
        {
            _databaseService.ProductProvider.Remove(productProvider);
            _databaseService.Save();
        }
    }
}
