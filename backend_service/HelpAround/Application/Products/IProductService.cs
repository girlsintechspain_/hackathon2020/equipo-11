﻿using Domain.Products;
using System.Collections.Generic;

namespace Application.Products
{
    public interface IProductService
    {
        public List<Product> GetAll();
        public Product Get(long id);
        public Product Add(Product product);
        public Product Update(Product product);
    }
}
