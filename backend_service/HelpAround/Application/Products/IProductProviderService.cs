﻿using Domain.Products;

namespace Application.Products
{
    public interface IProductProviderService
    {
        public ProductProvider Add(ProductProvider productProvider);
        public void Remove(ProductProvider productProvider);
    }
}
