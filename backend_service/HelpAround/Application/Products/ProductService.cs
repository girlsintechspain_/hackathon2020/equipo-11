﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Products
{
    public class ProductService : IProductService
    {
        private IDatabaseService _databaseService;
        public ProductService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Product Add(Product product)
        {
            _databaseService.Product.Add(product);
            _databaseService.Save();

            return product;
        }

        public List<Product> GetAll()
        {
            return _databaseService.Product.ToList();
        }

        public Product Get(long id)
        {
            var product = _databaseService.Product.Single(a => a.ProductId == id);
            return product;
        }

        public Product Update(Product product)
        {
            _databaseService.Product.Update(product);
            _databaseService.Save();

            return product;
        }
    }
}
