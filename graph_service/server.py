import uuid

from flask import Flask, jsonify, request
from flask_cors import CORS
from config import SERVER, NEO4J

from neo_interface import NeoInterface, UserNodeMixin

app = Flask(__name__)
CORS(app)

graph = NeoInterface(
    neo4j_host=NEO4J['host'],
    neo4j_port=NEO4J['port'],
    neo4j_user=NEO4J['user'],
    neo4j_password=NEO4J['password']
)


@app.route('/')
def home_view():
    return jsonify({
        'name': 'Graph-Service',
        'version': '0.0.1'
    })


@app.route('/user', methods=['POST'])
def create_user():
    form = request.form
    session = graph.get_session(graph.driver)
    user = UserNodeMixin()
    role = form.get('role')

    if not role:
        return jsonify({
            'message': 'Role is required'
        }), 400

    user_query = user.create_user(
        username=form['username'],
        user_id=form['user_id'],
        email=form['email'],
        lat=form['lat'],
        lon=form['lon']
    )

    res = session.run(
        """
        MATCH (u:User) 
        WHERE u.user_id = {user_id} 
        OR u.username = "{username}"
        OR u.email = "{email}" 
        RETURN count(u) AS user_count
        """.format(
            user_id=form['user_id'],
            username=form['username'],
            email=form['email']
        )
    )

    records = res.data()

    if len(records) > 0 and records[0]['user_count'] == 0:
        r = graph.run_query(session, user_query, True)
        response = None
        if role == 'dependant':
            r2 = session.run("""
                MERGE (d:Dependant {{ covid: {covid_status}, user_id: {user_id} }})
                WITH d MATCH (u:User) WHERE u.user_id = {user_id}
                WITH d,u MERGE (u)-[:HAS_ROLE]->(d) 
                RETURN {{user: u, dependant: d}} as profile
            """.format(
                covid_status=form.get('covid_status', False),
                user_id=form.get('user_id')
            ))
            response = r2.data()
        elif role == 'volunteer':
            r2 = session.run("""
                MERGE (v:Volunteer {{ active: False, user_id: {user_id} }})
                WITH v MATCH (u:User) WHERE u.user_id = {user_id}
                WITH v,u MERGE (u)-[:HAS_ROLE]->(v) 
                RETURN {{user: u, volunteer: v}} as profile
            """.format(
                user_id=form.get('user_id')
            ))
            response = r2.data()

        session.close()

        return jsonify({
            "message": 'Created',
            "data": response
        }), 201
    else:
        session.close()

        return jsonify({
            'message': 'Not created, user already exists'
        }), 400


@app.route('/login', methods=['POST'])
def login():
    form = request.form
    print(form)
    session = graph.get_session(graph.driver)
    res = session.run(
        """
        MATCH (u:User)-[:HAS_ROLE]->(r)
        WHERE u.username = "{username}"
        OR u.email = "{email}" 
        RETURN count(u) AS user_count
        """.format(
            username=form.get('username', ''),
            email=form.get('email', '')
        )
    )
    records = [r for r in res.data()]

    if len(records) > 0 and records[0]['user_count'] == 0:
        session.close()
        return jsonify({
            'message': 'Invalid credentials'
        }), 401

    res2 = session.run("""
        MATCH (u:User)-[:HAS_ROLE]->(r)
        WHERE u.username = "{username}"
        OR u.email = "{email}" 
        RETURN {{user:u, role: r, role_type: labels(r)[0]}} as profile
        """.format(
            username=form.get('username', ''),
            email=form.get('email', '')
        )
    )
    data = res2.data()[0]
    session.close()
    return jsonify({
        'message': 'Login successful',
        'token': uuid.uuid4().hex,
        'profile': data['profile']
    }), 200


@app.route('/market', methods=['POST'])
def add_market():
    form = request.form
    session = graph.get_session(graph.driver)

    market = session.run("""
        MERGE (m:Market {{
            name: "{name}",
            category: "{category}",
            lat: "{lat}",
            lon: "{lon}",
            market_id: {market_id}        
        }})
        RETURN m AS market
    """.format(
        name=form['name'],
        category=form['category'],
        lat=form['lat'],
        lon=form['lon'],
        market_id=form['market_id']
    ))

    return jsonify({
        'message': 'Created',
        'market': market.data()[0]['market']
    }), 201


@app.route('/markets', methods=['GET'])
def get_markets():
    session = graph.get_session(graph.driver)

    market_list = session.run("""
        MATCH (m:Market) Return {market: m, products: [(m)--(p:Product) | p]} as market
    """)

    return jsonify({
        'message': 'List',
        'markets': [m['market'] for m in market_list.data()]
    })


@app.route('/product', methods=['POST'])
def add_product():
    form = request.form

    session = graph.get_session(graph.driver)

    product_query = session.run("""
        MERGE (p:Product {{
            name: "{name}",
            price: {price},
            description: "{description}",
            product_id: "{product_id}"
        }})
        WITH p MATCH (m:Market) 
        WHERE m.market_id = {market_id}
        WITH p, m MERGE (p)-[:IS_SOLD_IN]->(m)
        RETURN {{product: p, market: m}} AS product
    """.format(
        name=form['name'],
        price=form['price'],
        description=form['description'],
        product_id=form['product_id'],
        market_id=form['market_id']
    ))

    return jsonify({
        'message': 'Created',
        'product': product_query.data()[0]['product']
    })


app.run(**SERVER)
