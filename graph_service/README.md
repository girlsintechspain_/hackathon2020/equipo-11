# Ángel Graph Service

### Hecho con Python (Flask)

## Importante
El servicio utiliza Neo4J como motor de BBDD de grafos, funciona perfectamente
con la versión community, puedes tenerla en local, como servicio (ofrecen instancias 
gratuitas teniendo cuenta) o desplegada en servidor.

Para evitar problemas configurando se incluye un contenedor Docker preparado para usar

```shell script
sudo docker run -p 7687:7687 -p 7474:7474 -d -t -v /PATH_TO_DATABASE_FOLDER/YOUR_DATABASE_FOLDER:/var/lib/neo4j/data/databases/graph.db jasonjimnz/neo4j-community
```

El argumento -v indica el volumen, puedes dejar un directorio vacío para mantener tus datos
fuera del contenedor, no es necesario pero sí recomendable

Con la instancia de Neo4J desplegada hay que hacer lo siguiente:

1. Entrar a la url (en local http://localhost:7474) 
2. Iniciar sesión, el host del protocolo bolt es el mismo que 
    el de la interfaz web pero su puerto por defecto es 7687, las
    credenciales de usuario son:
    - usuario: neo4j
    - contraseña: neo4j
3. Te pedirá que cambies la contraseña del admin
4. A partir del cambio puedes utilizar el cliente web de Neo4J y conectarte en remoto

# Dependencias

- flask: 1.1.2
    - Microframework para exponer el código como API REST
- flask-ors: 3.0.9
    - Plugin de Flask para que acepte peticiones desde cualquier
        URL
- neo4j-driver: 4.2.0
    - Conector de Neo4J para trabajar con el motor de BBDD
    
   
# Inicio rápido para desarrollo

Es necesario crear un fichero de configuración llamado config.py, puedes copiar
el fichero config.py.dist y ajustar los valores como prefieras

```python
SERVER = {
    'host': '0.0.0.0',
    'port': 5000,
    'debug': True
}

NEO4J = {
    "host": "localhost", # Your Neo4J URL
    "port": "7687", # Your Neo4J Bolt port
    "user": "neo4j", # Your Neo4J user
    "password": "neo4j" # Your Neo4J password
}
```

NOTA: Necesitas tener el fichero config.py con las credenciales correctas
de la instancia de Neo4J, dado que el servicio se conecta con el driver
al entrar en ejecución
 
```shell script
cd graph_service
pip3 install -r requirements.txt
python3 server.py
```

# TODOs
- Anclaje con el Bakcend principal para validar usuarios por token
- Gestión de errores en los formularios
- Aplicar vistas con los cálculos de distancias para reutilizar la geoposición
- Sistema de recomendación en base a productos que están en tiendas donde se ha comprado
- Habilitar vista de tareas

# Rutas

   - /
        - Método: GET
        - Descripción:
            Llamada que devuelve información sobre el servicio
        - Respuesta:
        ````json
             {
                "name": "Graph-Service",
                "version": "0.0.1"
             }
        ````
   - /user
        - Método: POST
        - Descripción:
            Llamada que añade un usuario al la BBDD de grafos
            en caso de no cumplir con los requerimientos en la llamada
            devolverá error, para el caso de los roles si no tiene un 
            valor dentro de los acpetados momentáneamente el usuario
            será creado pero no tendrá su rol (no podrá logarse)
        - Parámetros:
            - username:
                - tipo: string
                - requerido: si
            - email: 
                - tipo: string
                - requerido: si
            - user_id:
                - tipo: integer
                - requerido: si
            - lat:
                - tipo: string
                - requerido: si
            - lon: 
                - tipo: string
                - requerido: si
            - role: 
                - tipo: string
                - requerido: si
                - valores aceptados:
                    - dependant
                    - vounteer
        - Respuesta:
        ```json
             {
                 "data": [
                     {
                         "profile": {
                             "user": {
                                 "email": "jasonjimenezcruz+07@gmail.com",
                                 "lat": "40.307369",
                                 "lon": "-3.736841",
                                 "user_id": 7,
                                 "username": "jason07"
                             },
                             "volunteer": {
                                 "active": false,
                                 "user_id": 7
                             }
                         }
                     }
                 ],
                 "message": "Created"
             }
        ```
   - /login
        - Método: POST
        - Descripción:
            Busca al usuario en la BBDD de grafos y le asigna un token 
            (aun no guardado, en BBDD)
        - Parámetros:
            - username:
            - tipo: string
            - requerido: no
        - email:
            - tipo: string
            - requerido: no 
        - Respuesta:
            ```json
                {
                    "message": "Login successful",
                    "profile": {
                        "role": {
                            "active": false,
                            "user_id": 1
                        },
                        "role_type": "Volunteer",
                        "user": {
                            "email": "jasonjimenezcruz@gmail.com",
                            "lat": "40.307369",
                            "lon": "-3.736841",
                            "user_id": 1,
                            "username": "jason"
                        }
                    },
                    "token": "49b391d9320645f6856a3cab655e297c"
                }
            ```
   - /market
        - Método: POST
        - Descripción:
            Añade una tienda a la BBDD
        - Parámetros:
            - name:
                - tipo: string
                - requerido: si
            - category:
                - tipo: string
                - requerido: si
            - lat:
                - tipo: string
                - requerido: si
            - lon:
                - tipo: string
                - requerido: si
            - market_id:
                - tipo: int
                - requerido: si
        - Respuesta:
            ```json
                {
                    "market": {
                        "category": "Farmacia",
                        "lat": "40.307369",
                        "lon": "-3.736841",
                        "market_id": 100,
                        "name": "Farmacia Saludable"
                    },
                    "message": "Created"
                }
            ```
   - /markets
        - Método: GET
        - Descripción:
            Devuelve el listado de tiendas y productos asociados existentes
            en la bbdd
        - Parámetros:
        - Respuesta:
            ```json
            {
                "markets": [
                    {
                      "market": {
                          "category": "tienda",
                          "lat": "40.473907649051704",
                          "lon": "-3.5855350682202047",
                          "market_id": 1,
                          "name": "Tienda 101"
                      },
                      "products": [
                          {
                              "description": "Botella de agua",
                              "name": "Botella Agua 50cl",
                              "price": 0.35,
                              "product_id": "00000000003"
                          }
                      ]
                    },
                    {
                      "market": {
                          "category": "tienda",
                          "lat": "40.36815527702884",
                          "lon": "-3.7458547551539705",
                          "market_id": 2,
                          "name": "Tienda 102"
                      },
                      "products": [
                          {
                              "description": "Brick leche",
                              "name": "Brick Leche 1L",
                              "price": 0.75,
                              "product_id": "00000000004"
                          }
                      ]
                    },
                    {
                      "market": {
                          "category": "tienda",
                          "lat": "40.45887476564644",
                          "lon": "-3.6664670709154676",
                          "market_id": 3,
                          "name": "Tienda 103"
                      },
                      "products": [
                          {
                              "description": "Pan de molde sin lguten",
                              "name": "Pan de molde sin gluten 900gr",
                              "price": 1.6,
                              "product_id": "00000000005"
                          }
                      ]
                    },
                    {
                      "market": {
                          "category": "Alimentacion",
                          "lat": "40.307369",
                          "lon": "-3.736841",
                          "market_id": 1,
                          "name": "Panadería Carmen"
                      },
                      "products": [
                          {
                              "description": "Botella de agua",
                              "name": "Botella Agua 50cl",
                              "price": 0.35,
                              "product_id": "00000000003"
                          },
                          {
                              "description": "Barra de pan pistola",
                              "name": "barra pistola",
                              "price": 0.45,
                              "product_id": "00000000002"
                          },
                          {
                              "description": "Barra de pan bagette",
                              "name": "pan bagette",
                              "price": 0.6,
                              "product_id": "00000000001"
                          }
                      ]
                    },
                    {
                      "market": {
                          "category": "Farmacia",
                          "lat": "40.307369",
                          "lon": "-3.736841",
                          "market_id": 100,
                          "name": "Farmacia Saludable"
                      },
                      "products": []
                    },
                    {
                      "market": {
                          "category": "Farmacia",
                          "lat": "40.307319",
                          "lon": "-3.736811",
                          "market_id": 2,
                          "name": "Farmacia Madrid"
                      },
                      "products": [
                          {
                              "description": "Brick leche",
                              "name": "Brick Leche 1L",
                              "price": 0.75,
                              "product_id": "00000000004"
                          }
                      ]
                    }
                ],
                "message": "List"
                }
            ```
   - /product
        - Método: POST
        - Descripción:
            Añade un producto a una tienda
        - Parámetros:
            - name:
                type: string
                required: true
            - price:
                type: float
                required: true
            - description:
                type: string
                required: true
            - product_id:
                type: string
                required: true
            - market_id:
                type: int
                required: true
        - Respuesta:
        ```json
            {
                "message": "Created",
                "product": {
                    "market": {
                        "category": "tienda",
                        "lat": "40.473907649051704",
                        "lon": "-3.5855350682202047",
                        "market_id": 1,
                        "name": "Tienda 101"
                    },
                    "product": {
                        "description": "Pan de molde sin lguten",
                        "name": "Pan de molde sin gluten 900gr",
                        "price": 1.65,
                        "product_id": "0000000000"
                    }
                }
            }
        ```
    