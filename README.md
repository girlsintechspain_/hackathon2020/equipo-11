# Ángel:  Hacking For Humanity Team 11

Ángel es una aplicación que quiere acercar a voluntarios, de su comunidad 
con las personas que se han visto privadas de poder salir de casa y 
que tienen a sus seres queridos lejos debido a las restricciones 
en la movilidad entre ciudades.

[Licencia MIT](./LICENSE)

## Proyecto
El proyecto consta de 3 servicios que trabajan en conjunto:
 
- [Backend](./backend_service/Readme.md)
    - Desarrollado en .NET Core y con SQL Server como BBDD
- [Frontend](./frontend/README.md)
    - Desarrollado en ReactJS con previsión de la reutilización
        de los componentes para una App híbrida con React Native
- [Graph Service](./graph_service/README.md)
    - Desarrollado en Python utilizando Flask y Neo4J como BBDD NoSQL
        que se encarga de las operaciones de cálculo de distancias y 
        sistema de recomendación, es un complemento del Backend.
        

## Recursos
   - [Personas](https://www.figma.com/file/4oO5Zn66y4JNtMxGMgdXgg/Personas-Angel-APP?node-id=0%3A1)
   - [Prototipo](https://www.figma.com/proto/lHcpzxTyzjGVSrDyrPaBtf/Angel?node-id=20%3A1&scaling=min-zoom)