import React, {useState} from "react";
import MapComponent from "../MapComponent";


const Market = ({market, ...props}) => {
    const [showMap, setShowMap] = useState(false)

    return <div className={'card col-3'}>
        <div className="card-body">
            <h5 className="card-title">{market.market.name}</h5>
            <div>
                <p>Categoria: {market.market.category}</p>
                <button className={'btn btn-primary'} onClick={props.openCallback}>Abrir</button>
                <h6 onClick={(e) => {setShowMap(!showMap)}}>{showMap ? 'Ocultar' : 'Mostrar'} mapa del establecimiento</h6>
                {showMap ? <MapComponent
                    isMarkerShown
                    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{height: `100%`}}/>}
                    containerElement={<div style={{height: `400px`}}/>}
                    mapElement={<div style={{height: `100%`}}/>}
                    markers={[market]} /> : ''}
            </div>
        </div>
    </div>
}

export default Market;