import React, {useState} from "react";
import {Redirect, withRouter} from "react-router-dom";

const Home = ({...props}) => {
    const [session, setSession] = useState(window.localStorage.getItem('session'));
    if (session)
        return <Redirect to={"/dashboard"} />;

    return <div className="container-fluid">
        <Redirect to={"/login"} />
    </div>
};

export default withRouter(Home);