import React from "react";
import {compose, withProps} from "recompose";
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from "react-google-maps";

const MapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `400px`}}/>,
        mapElement: <div style={{height: `100%`}}/>,
    }),
    withScriptjs,
    withGoogleMap
)((props) => (
    <GoogleMap
        defaultZoom={18}
        defaultCenter={{lat: 40.307382, lng: -3.736842}}
    >

        {props.isMarkerShown ? props.markers.map((m) => (<Marker label={m.market.name} position={{lat: parseFloat(m.market.lat), lng: parseFloat(m.market.lon)}}/>)) : ''}
    </GoogleMap>
));

export default MapComponent;