import React, {useState} from "react";
import {withRouter} from "react-router-dom";
import MapComponent from "../MapComponent";
import ajax from "superagent";
import Markets from "../Markets/Markets";
import Market from "../Markets/Market";

const Dashboard = ({...props}) => {
    const [markets, setMarkets] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [market, setMarket] = useState({name: null, products: [], category: null})
    function getMarkets(){
        const marketsCall = ajax.get('http://h4h2020-t11-graph-service.northeurope.cloudapp.azure.com:5000/markets').set('Access-Control-Allow-Origin', '*')

        marketsCall.then((response) => {
            setMarkets(response.body.markets)
        })
    }

    if (!markets)
        getMarkets();

    return <div className="container-fluid">
        <h2>Inicio</h2>
        {markets ? <MapComponent
            isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{height: `20vh`}}/>}
            containerElement={<div style={{height: `20vh`}}/>}
            mapElement={<div style={{height: `20vh`}}/>}
            markers={markets}
        /> : ''}
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">Listado de tiendas</h5>
                <div className="card-text">
                    <div className="row">
                        {markets ? markets.map((m) => {
                            console.log(m)
                            return <Market market={m} openCallback={() => {
                                setMarket(Object.assign(
                                    {},
                                    m.market,
                                    {products: m.products}
                                ));
                                setShowModal(true);
                            }} />}) : ''}
                    </div>
                </div>
            </div>
        </div>
        {showModal ? <div className="modal-backdrop" style={{
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100vw',
            height: '100vh',
            backgroundColor: 'rgba(0,0,0,0.6)'
        }}>
            <div className="modal" tabIndex="-1" role="dialog" style={{display: 'block'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{market.name}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={(e) => {setShowModal(false)}}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{market.category}</p>
                            <h6>Productos</h6>
                            {market.products.map((p) => (
                                <p>{p.name} - precio: {p.price}€</p>
                            ))}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">Pedir</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal"
                             onClick={(e) => {setShowModal(false)}}>Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> : ''}
    </div>
};

export default withRouter(Dashboard);