import React, {useState} from "react";
import {Redirect, withRouter, Link} from "react-router-dom";
import ajax from "superagent";


const Login = ({...props}) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [session, setSession] = useState(window.localStorage.getItem('session'));
    if (session)
        return <Redirect to={"/dashboard"} />;

    function submitLogin(e) {
        e.preventDefault();
        const loginRequest = ajax.post('http://h4h2020-t11-graph-service.northeurope.cloudapp.azure.com:5000/login').send({
            username: username
        }).set('Content-Type', 'application/x-www-form-urlencoded').set('Access-Control-Allow-Origin', '*')

        loginRequest.then((response) => {
            window.localStorage.setItem('session', response.body.token)
            window.localStorage.setItem('user', JSON.stringify(Object.assign(
                {},
                response.body.profile.user,
                {
                        role: Object.assign(
                            {},
                            response.body.profile.role,
                            response.body.profile.role_type
                        )
                    }
                )
            ));
            setSession(response.body.token);
        })
    }

    return <div className="container-fluid">
        <div className="card" style={{
            marginTop: '25vh',
            width: '50vw',
            marginLeft: 'auto',
            marginRight: 'auto'
        }}>
            <div className="card-body">
                <h5 className="card-title">Login</h5>
                <div className="card-text">
                    <form onSubmit={submitLogin}>
                        <div className="form-group">
                            <label htmlFor="username">Nombre de usuario </label>
                            <input type="text" className="form-control" id={"username"}
                                   placeholder="Nombre de usuario"
                                   value={username}
                                   onChange={(e) => {
                                       setUsername(e.target.value)
                                   }}/>
                            <small id="emailHelp" className="form-text text-muted">Puedes iniciar sesión con tu nombre de usuario</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Contraseña</label>
                            <input type="Contraseña" className="form-control" id="password"
                                   placeholder="Password"
                                   value={password}
                                   onChange={(e) => {
                                       setPassword(e.target.value)
                                   }}/>
                        </div>
                        <button type="submit" className="btn btn-primary">Iniciar sesión</button>
                        <Link to={"/register"} className="btn btn-primary" style={{marginLeft: '1rem'}}>Registrarse
                        </Link>
                    </form>
                </div>
            </div>
        </div>
    </div>
}


export default withRouter(Login);